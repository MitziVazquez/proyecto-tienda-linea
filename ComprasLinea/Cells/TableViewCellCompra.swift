//
//  TableViewCellCompra.swift
//  ComprasLinea
//
//  Created by MAC on 12/22/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import CoreData

class TableViewCellCompra: UITableViewCell {
    

    @IBOutlet weak var lblProducto: UILabel!
    @IBOutlet weak var lblPrecio: UILabel!
    @IBOutlet weak var lblCantidad: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var imgProductoCompra: UIImageView!
    
    var producto:[NSManagedObject] = []
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
}
