//
//  TableViewCellProducto.swift
//  ComprasLinea
//
//  Created by MAC on 12/19/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import CoreData

protocol Celda {
    
    func agregar(producto: Product, lbl: UILabel)
}

class TableViewCellProducto: UITableViewCell {
    
    @IBOutlet weak var imgProducto: UIImageView!
    @IBOutlet weak var lblProducto: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblPrecio: UILabel!
    @IBOutlet weak var lblCantidad: UILabel!
    @IBOutlet weak var StpContador: UIStepper!
    var lista:[NSManagedObject] = []
    var delegate: Celda?
    var producto: Product?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnAgregarCarrito(_ sender: UIButton) {
        
        /*guard let nombreProducto = lblProducto.text, nombreProducto != "", let precio = lblPrecio.text, precio != "", let cantidad = lblCantidad.text, cantidad != "", let stock = lblStock.text, stock != "" else {return}*/
        delegate?.agregar(producto: producto!, lbl: lblCantidad!)
        
        //self.guardarProducto(producto: nombreProducto, precio: Float(precio), cantidad: Int16(cantidad))
    
        
    }
    @IBAction func stpContar(_ sender: UIStepper) {
      var number = 0
        number = Int(sender.value)
        self.lblCantidad.text = String(number)
        StpContador.minimumValue = 1.0
        StpContador.maximumValue = Double(lblStock.text!)!
        
        
    }
    
        override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /*func guardarProducto(producto: String,precio: Float?,cantidad: Int16?) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
         let entity = NSEntityDescription.entity(forEntityName: "Compra", in: managedContext)!
         let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
     
         print(producto)
         managedObject.setValue(producto, forKeyPath: "nombreProducto")
         managedObject.setValue(precio, forKeyPath: "precio")
         managedObject.setValue(cantidad, forKeyPath: "cantidad")
        
        
         do{
            
             try managedContext.save()
            lista.append(managedObject)
        
             
         }catch let error as NSError {
             print("\(error.userInfo)")
     }
     
     
    }*/
    
}
