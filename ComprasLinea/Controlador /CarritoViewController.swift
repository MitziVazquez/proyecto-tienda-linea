//
//  CarritoViewController.swift
//  ComprasLinea
//
//  Created by MAC on 12/20/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import CoreData

class CarritoViewController: UIViewController {
    
    @IBOutlet weak var lblTotal: UILabel!
    var total: Double = 0.0
    @IBOutlet weak var tablaComprar: UITableView!
    var compra:[NSManagedObject] = []
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tablaComprar.delegate = self
        self.tablaComprar.dataSource = self
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "Compra") //trbajar sobre esta entidad, condicion que se quiere buscar
        do{
           
        
            let compra1 = try managedContext.fetch(fetchrequest)
            for datos in compra1 {
                self.compra.append(datos)
            }
            
        }catch let error as NSError{
            print("\(error.userInfo)")
        }
        
      
        
    }


}

extension CarritoViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return compra.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell = tableView.dequeueReusableCell(withIdentifier: "producto", for: indexPath) as! TableViewCellCompra
        
        
    let compras = compra[indexPath.row]
    cell.lblProducto.text = compras.value(forKey:"nombreProducto") as? String
    cell.lblPrecio.text = "\(compras.value(forKey: "precio")!)"
    cell.lblCantidad.text = "\(compras.value(forKey: "cantidad")!)"
       
        var calcular = Double(cell.lblSubtotal.text!)
        let cantidad = Int16(cell.lblCantidad.text!)!
        print(cantidad)
        let precio = Double(cell.lblPrecio.text!)!
        print(precio * Double(cantidad))
        
        calcular = (precio * Double(cantidad))
        
        cell.lblSubtotal.text = "Subtotal $ \(String(describing: calcular!))"
        
        total += calcular!
        lblTotal.text = "Total a Pagar  $\(total)"
     
            return cell
           
        }
        
        func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            //self.lista.remove(at: indexPath.row)
            
            // Delete the row from the data source
            let objcore = compra[indexPath.row]
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let managedContext = appDelegate!.persistentContainer.viewContext
            
            managedContext.delete(objcore) // elimina del coredata
            compra.remove(at: indexPath.row) // lo elimina del arreglo
            self.basica(title: "Atencion", message: "Desea eliminar el producto del carrito")
            total = 0.0
            DispatchQueue.main.async {
                 self.tablaComprar.reloadData()
            }
           
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 155
        }

}
