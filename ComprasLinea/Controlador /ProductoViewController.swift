//
//  ProductoViewController.swift
//  ComprasLinea
//
//  Created by MAC on 12/19/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import CoreData

class ProductoViewController: UIViewController {

    
    @IBOutlet weak var tablaProductos: UITableView!
    var productos: Array<Product> = []
    var lista:[NSManagedObject] = []
    
    var bandera:Bool = true
    
    
    
    @IBAction func Stepper(_ sender: UIStepper) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tablaProductos.delegate = self
        tablaProductos.dataSource = self
        self.tablaProductos.reloadData()
        mostrarProductos()
        
        
        //llamado a appDelegate
    }
    @IBAction func btnComprar(_ sender: UIButton) {
    
        performSegue(withIdentifier: "carrito", sender: nil)
        
    }

    @IBAction func closeSessionTapped(_ sender: UIBarButtonItem) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "Compra") //trbajar sobre esta entidad, condicion que se quiere buscar
        do{
               
       let compra1 = try managedContext.fetch(fetchrequest)
        for datos in compra1 {
                           //compra.append(datos)
           managedContext.delete(datos)
                       }
                       
        }catch let error as NSError{
            print("\(error.userInfo)")
        }
        self.navigationController?.popToRootViewController(animated: true)
    }

    func mostrarProductos(){
        Producto(completions: { listaproducto in self.productos = listaproducto
            DispatchQueue.main.async {
                self.tablaProductos.reloadData()
        }
    })
  }
    
}


extension ProductoViewController: UITableViewDelegate, UITableViewDataSource{
    
    
   /* func updateSearchResults(for searchController: UISearchController) {
        
    }
    
   func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
//    self.auxproducto = self.productos.filter { (exercise: String) -> Bool in
//
//      if exercise.lowercased().contains(self.Busqueda.text!.lowercased()){
//                     return true
//                 }else {
//                     return false
//                 }
//             }
//
//             if(searchText != ""){
//                 self.bandera = false
//                 self.tablaProductos.reloadData()
//             }else {
//                 self.bandera = true
//                 self.tablaProductos.reloadData()
//             }
         }*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.productos.count
        
    }

        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! TableViewCellProducto
       
        
        let product = productos[indexPath.row]
        let imgProducto = self.ConvertBase64StringToImage(imageBase64String: product.img)

                cell.lblProducto.text = (product.nombreProducto)
                cell.lblPrecio.text = String(product.precio)
                cell.lblStock.text = String(product.stock)
                cell.imgProducto.image = imgProducto
        cell.producto = product
        cell.delegate = self
        
         return cell
        
        /*let producto: Product = productos[indexPath.row]
        cell.lblProducto.text = producto.producto
        cell.lblCantidad.text = String(producto.cantidad)
        cell.lblPrecio.text = String(producto.precio)
        cell.lblStock.text = String(producto.stock)
        cell.imgProducto.image = self.ConvertBase64StringToImage(imageBase64String: producto.imagen)*/
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
          let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 1))
          let image = UIImage(data: imageData!)
          return image!
      }
    
        
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155
    }
    
    func guardarProducto(producto: String,precio: Float?,cantidad: Int16?) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
         let entity = NSEntityDescription.entity(forEntityName: "Compra", in: managedContext)!
         let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
     
         print(producto)
         managedObject.setValue(producto, forKeyPath: "nombreProducto")
         managedObject.setValue(precio, forKeyPath: "precio")
         managedObject.setValue(cantidad, forKeyPath: "cantidad")
        
        
         do{
            
             try managedContext.save()
            lista.append(managedObject)
        
             
         }catch let error as NSError {
             print("\(error.userInfo)")
     }
     
     
    }
    
    
    
}

extension ProductoViewController: Celda {
  
    func agregar(producto: Product, lbl:UILabel) {
      
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "Compra")
        
       do{
        
            let compra = try managedContext.fetch(fetchrequest)
            for datos in compra {
                self.lista.append(datos)
            }
            
        }catch let error as NSError{
            print("\(error.userInfo)")
        }
        
        var bandera = 1
        
        for nombre in lista {
            if nombre.value(forKey: "nombreProducto") as? String == producto.nombreProducto {
                
                let aux:Int16 = (nombre.value(forKey: "cantidad") as? Int16)!
                let aux1:Int16 = Int16(lbl.text!)!
                print("Cantidad1:\(aux)\(aux1)")
                let result = (aux+aux1)
                nombre.setValue(result, forKey: "cantidad")
                bandera = 0
                basica(title: "Atencion", message: "Agrego un producto al carrito")
            
                    do{
                    try managedContext.save()
                    
                }catch let error as NSError{
                    print("\(error.userInfo)")
                }
            }
            
        }
        
        if bandera == 1 {
            
            print("Nombre: \(producto.nombreProducto)")
            self.guardarProducto(producto: producto.nombreProducto, precio: producto.precio, cantidad: Int16(lbl.text!)!)
            basica(title: "Atencion", message: "Agrego un producto al carrito")
        }
        
        
    }
    
    
}
