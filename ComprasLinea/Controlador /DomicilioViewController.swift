//
//  DomicilioViewController.swift
//  ComprasLinea
//
//  Created by MAC on 12/22/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class DomicilioViewController: UIViewController {
    @IBOutlet weak var txtCalle: UITextField!
    @IBOutlet weak var txtExterior: UITextField!
    @IBOutlet weak var txtInterior: UITextField!
    @IBOutlet weak var txtCiudad: UITextField!
    @IBOutlet weak var txtColonia: UITextField!
    @IBOutlet weak var txtcodigoPostal: UITextField!
    @IBOutlet weak var txtMunicipio: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
    }
    
    
    @IBAction func btnGurdarDomicilio(_ sender: UIButton) {

        guard let calle = self.txtCalle.text, let interior = self.txtInterior.text, let exterior = self.txtExterior.text, let codigoPostal = self.txtcodigoPostal.text ,let colonia = self.txtColonia.text,let municipio = self.txtMunicipio.text,let ciudad = self.txtCiudad.text else {return}
        
        
      guardarDomicilio(calle: calle, interior: interior, exterior: exterior, codigoPostal: codigoPostal, municipio: municipio, colonia: colonia, ciudad: ciudad)
    }

    func guardarDomicilio(calle:String, interior: String, exterior: String,codigoPostal: String,municipio: String,colonia: String,ciudad: String){
        
        if validar(calle: calle, numeroInterior: interior, numeroExterior: exterior, codigoPostal: codigoPostal, municipio: municipio, colonia: colonia, ciudad: ciudad) {
            
        let domicilio: Domicilio = Domicilio(dic: ["calle": calle,"numeroInterior": interior,"numeroExterior": exterior,"codigoPostal": codigoPostal, "municipio": municipio,"colonia": colonia,"ciudad": ciudad])
            
               agregarDomicilio(domicilio: domicilio, completions: { success, message  in
                   print("message completions -> \(message)")
                   if success {
                      DispatchQueue.main.async {
                       self.accion(title: "Usuario", message: message , actionButton: {
                           let story = UIStoryboard(name: "Main", bundle: nil)
                           let control = story.instantiateViewController(identifier: "tarjeta") as! PagoViewController
                           self.present(control,animated: true, completion: nil)
                           //unwind segue
                                       })
                       }
                   } else {
                       DispatchQueue.main.async{
                       self.basica(title: "Atención", message: message)
                       }
                       
                   }
       })
        }else {
            DispatchQueue.main.async{
            self.basica(title: "Atención", message: "No se puede enviar el domicilio")
        }

       }
    }
    
    func validar (calle: String, numeroInterior: String ,numeroExterior: String,codigoPostal: String,municipio: String,colonia: String,ciudad: String) -> Bool {
    
    
        guard let calle = self.txtCalle.text, calle != "" else {
        self.basica(title: "Atención", message: "Complete el campo de la calle")
       return false }
        
        guard calle.validarTexto else {
         self.basica(title: "Atención", message: "no es una calle")
        return false }
        
        guard let codigoPostal = self.txtcodigoPostal.text, codigoPostal != "" else {
         self.basica(title: "Atención", message: "Complete el campo codigo postal")
        return false }
        
        guard codigoPostal.count == 5  else {
         self.basica(title: "Atención", message: "Error en el codigo postal son 5 caracteres")
        return false }
        
        guard let noInterior = self.txtInterior.text, noInterior != "" else {
         self.basica(title: "Atención", message: "Complete el campo numero Interior")
        return false }
        
        
        guard let noExterior = self.txtExterior.text, noExterior != "" else {
         self.basica(title: "Atención", message: "Complete el campo del numero Exterior")
        return false }
        
        guard let municipio = self.txtMunicipio.text, municipio != "" else {
         self.basica(title: "Atención", message: "Complete el campo municipio")
        return false }
        
        guard municipio.validarTexto else {
         self.basica(title: "Atención", message: "No es un municipio valido")
        return false }
        
        guard let colonia = self.txtColonia.text, colonia != "" else {
         self.basica(title: "Atención", message: "Complete el campo colonia")
        return false }
        
        guard colonia.validarTexto else {
         self.basica(title: "Atención", message: "No es una Colonia valida")
        return false }
        
        guard let ciudad = self.txtCiudad.text, ciudad != "" else {
         self.basica(title: "Atención", message: "Complete el campo ciudad")
        return false }
        
        guard ciudad.validarTexto else {
         self.basica(title: "Atención", message: "No es una Ciudad valida")
        return false }
    
       return true
    }
}
