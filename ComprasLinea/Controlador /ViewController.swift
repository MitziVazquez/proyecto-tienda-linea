//
//  ViewController.swift
//  ComprasLinea
//
//  Created by MAC on 12/18/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import LocalAuthentication
import CryptoKit
import CoreData

class ViewController: UIViewController {
    
   
    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtContrasena: UITextField!
    @IBOutlet weak var btnBiometricos: UIButton!
    var compra:[NSManagedObject] = []
    // la parte biometrica es la que se encarga de registrar en el sistema operativo
    let context = LAContext()
    var error: NSError?
    var strAlertMessage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "Compra") //trbajar sobre esta entidad, condicion que se quiere buscar
            do{
        
                let compra1 = try managedContext.fetch(fetchrequest)
                for datos in compra1 {
                    //compra.append(datos)
                    managedContext.delete(datos)
                }
                
            }catch let error as NSError{
                print("\(error.userInfo)")
            }
    if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,error: &error){
            
                switch context.biometryType {
                case .faceID:
                    btnBiometricos.setImage(UIImage(named: "facial"),for: UIControl.State.normal) // crea una imagen apartir de un nombre especifico
                    self.strAlertMessage = "Desea identificarse con FACEID ?"
                    break
                case .touchID:
                    
                    btnBiometricos.setImage(UIImage(named: "huella"),for: UIControl.State.normal)
                    self.strAlertMessage = "Desea identificarse con TOUCHID ?"
                    break
                case .none:
                    
                    print("Dispositivo sin sensor biometrico")
                }
                
            }else{
               
               //dispositivo no tinene sensor biometrico
                if let err = error {
                    
                    let strMessage = self.errorMessage(errorCode: err._code)
                    self.notifyUser("error", error: strMessage)
             
                }
        }
        
    }
            
            func errorMessage(errorCode:Int) -> String{
             
                var strMessage = ""
                switch errorCode {
                case LAError.authenticationFailed.rawValue:
                    
                    strMessage = "Authentication Failed"
                    
                    break
                case LAError.userCancel.rawValue:
                    
                    strMessage = "User Cancel"
                    
                    break
                case LAError.userFallback.rawValue:
                    
                    strMessage = "User Fallback"
                    
                case LAError.systemCancel.rawValue:
                    
                    strMessage = "System Cancel"
                    
                    break
                case LAError.passcodeNotSet.rawValue:
                    
                    strMessage = "Passcode Not set "
                    break
                case LAError.biometryNotAvailable.rawValue:
                    
                    strMessage = "TouchID not Available"
                    
                    break
                case LAError.appCancel.rawValue:
                    
                    strMessage = "appCancel"
                    
                    break
                case LAError.biometryLockout.rawValue:
                    
                    strMessage = "Lockout"
                    
                    break
                default:
                    
                    strMessage = ""
                
                }
                return strMessage
                
            }
        
        func notifyUser(_ msg:String, error:String?){
            let alert = UIAlertController(title: msg, message: error, preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        
    
    @IBAction func btnIdentificar(_ sender: UIButton) {
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                       localizedReason: self.strAlertMessage,
                                       reply: { [unowned self] (success, error) -> Void in
                                        
                DispatchQueue.main.async {
                                            
                if (success){
                print("Identificado Correctamente")
                //aqui va la vista a la que se desea ir
//                 let story = UIStoryboard(name: "Main", bundle: nil)
//                 let control = story.instantiateViewController(identifier: "producto") as! ProductoViewController
//                    self.navigationController?.pushViewController(control, animated: true)
//
                    self.performSegue(withIdentifier: "productos", sender: nil)
                } else  {
                    self.basica(title: "Error", message: "No identificado")
                print("No identificado")
                                                
                if let error = error {
                                                     
                let strMessage = self.errorMessage(errorCode: error._code)
                self.notifyUser("Error", error: strMessage)
               }
             }
           }
         })
       }
    }

    @IBAction func btnRegistrar(_ sender: UIButton) {
       let story = UIStoryboard(name: "Main", bundle: nil)
        let control = story.instantiateViewController(identifier: "registro") as! RegistroViewController
         self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnEntrar(_ sender: UIButton) {
        guard let usuario = self.txtUsuario.text, let contraseña = self.txtContrasena.text else {return}
       Iniciar (usuario: usuario, contraseña: contraseña)
        
    }

  /*  override func unwind(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
        ....
    }*/
    
    func Iniciar (usuario:String, contraseña: String){
        if validar(usuario: usuario, contrasena: contraseña) {
            
            let ingresa: Ingresar = Ingresar(dic: ["correo": usuario, "password": cifrarPassword(contrasena: contraseña)])
            
            self.showActivityIndicatory(uiView: self.view)
            
            iniciarSesion(ingresar: ingresa, completions: { success, message  in
                print("message completions -> \(message)")
                
                DispatchQueue.main.async {
                
                self.hideActivityIndicator(uiView: self.view)
                if success {
                self.accion(title: "Usuario", message: message , actionButton: {
                /*let story = UIStoryboard(name: "Main", bundle: nil)
                let control = story.instantiateViewController(identifier: "producto") as! ProductoViewController
                self.present(control,animated: true, completion: nil)*/
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let control = story.instantiateViewController(identifier: "producto") as! ProductoViewController
                     self.navigationController?.pushViewController(control, animated: true)
                                    //unwind segue
           })
     } else {
        
         self.basica(title: "Atención", message: message)
      }
     }
   })
 }
}

   /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "registro":
            let destination = segue.destination as! RegistroViewController
            present(destination, animated: true,completion: nil)
        case "principal":
            let destination = segue.destination as! ProductoViewController
            present(destination, animated: true, completion: nil)
        default:
            break
        }
    }*/
    
    func validar (usuario: String, contrasena: String) -> Bool {

     guard let usuario = self.txtUsuario.text, usuario != "" else {
        self.basica(title: "Atención", message: "Complete el campo correo")
        return false }

     guard usuario.validarEmail else {
        self.basica(title: "Atención", message: "Verifique que el campo correo contenga los siguientes caracteres ejemplo@dominio.com")
        return false }
               
       guard let contraseña = self.txtContrasena.text, contraseña != "" else {
         self.basica(title: "Atención", message: "Complete el campo contraseña")
         return false }
               
       guard contraseña.count >= 8 else {
          self.basica(title: "Atención", message: "son 8 caracteres minimos en  la contraseña")
               return false }
       return true
    }
    
    func cifrarPassword(contrasena:String) -> String{
        let password = Data(contrasena.utf8)
         let has = SHA256.hash(data: password)
        return has.compactMap { String(format: "%02x", $0) }.joined()
    }

}

