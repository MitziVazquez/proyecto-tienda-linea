//
//  RegistroViewController.swift
//  ComprasLinea
//
//  Created by MAC on 12/20/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import CryptoKit

class RegistroViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var sgValidarIdentificacion: UISegmentedControl!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellidoPaterno: UITextField!
    @IBOutlet weak var txtApellidoMaterno: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtContraseña: UITextField!
    @IBOutlet weak var txtConfirmar: UITextField!
     
    @IBOutlet weak var btnSelecionar1: UIButton!
    @IBOutlet weak var btnSeleccionar2: UIButton!
    let image: UIImagePickerController = UIImagePickerController()

    @IBOutlet weak var imgFoto1: UIImageView!
    @IBOutlet weak var imgFoto2: UIImageView!
    var tipoIdentificacion = ""
    
    
    var bandera: Bool = false
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //pobladoTest()
        
        image.delegate = self
    }
    
       /*private func pobladoTest(){
        txtNombre.text = "Mitzi"
        txtApellidoPaterno.text = "Vazquez"
        txtApellidoMaterno.text = "Laparra"
        txtCorreo.text = "mitzi@baz.com"
        txtContraseña.text = "minombreestabamal"
        txtConfirmar.text = "minombreestabamal"
    }*/
    
    
    @IBAction func btnSeleccionarFoto1(_ sender: AnyObject) {
         bandera = false
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                image.allowsEditing = false
                image.sourceType = .photoLibrary
                present(image, animated: true, completion: nil )
                }
        
           }
       
    
    @IBAction func btnSeleccionarFoto2(_ sender: AnyObject) {
        bandera = true
      if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                image.allowsEditing = false
                image.sourceType = .photoLibrary
                
                present(image, animated: true, completion: nil )
            }
        }
    
    @IBAction func btnTomarFoto1(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                image.allowsEditing = false
                image.sourceType = .camera
                image.cameraCaptureMode = .photo
                
                present(image, animated: true, completion: nil )
            }
        }else {
            
          basica(title: "ERROR", message: "no se puede abrir la camara")
    }
}
    
    @IBAction func btnFoto2(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
                    if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                        image.allowsEditing = false
                        image.sourceType = .camera
                        image.cameraCaptureMode = .photo
                        
                        present(image, animated: true, completion: nil )
                    }
                }else {
                    
                  basica(title: "ERROR", message: "no se puede abrir la camara")
            }
        }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         if let imagenSeleccionada: UIImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if bandera == false {
                imgFoto1.image = imagenSeleccionada
                
            }else if bandera == true {
                imgFoto2.image = imagenSeleccionada
            }
             if image.sourceType == .camera {
                 UIImageWriteToSavedPhotosAlbum(imagenSeleccionada, nil,nil,nil)
             }
         }
        image.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sgmIdentificar(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.tipoIdentificacion = "INE"
        case 1:
            self.tipoIdentificacion = "IFE"
        default:
            self.tipoIdentificacion = ""
            
        }
        
    }
    
    @IBAction func btnGuardar(_ sender: UIButton) {
        guard let nombre = self.txtNombre.text, let apellidoP = self.txtApellidoPaterno.text, let apellidoM = self.txtApellidoMaterno.text, let correo = self.txtCorreo.text, let contraseña = txtContraseña.text,let foto1 = imgFoto1.image, let foto2 = imgFoto2.image else {return}
        let img1 = self.resize(foto1)
        let img2 = self.resize(foto2)

        guardarUsuaurio(nombre: nombre, apellido_pat: apellidoP, apellido_mat: apellidoM, correo: correo, password: contraseña, foto1: img1, foto2: img2 )
    }
    
    func guardarUsuaurio(nombre:String, apellido_pat: String, apellido_mat: String, correo: String, password: String, foto1: UIImage, foto2: UIImage){
            
        if validar(nombre: nombre, apellido_pat: apellido_pat, apellido_mat: apellido_mat, correo: correo, contraseña: password,imagen: foto1) {
            
            //guard let delantera = imgFoto1.image else {return}
           // guard let trasera = imgFoto2.image else {return}
             
            
            let usuario: Usuario = Usuario(dic: ["nombre": nombre, "apellidoPaterno": apellido_pat, "apellidoMaterno": apellido_mat, "correo": correo, "password": cifrarPassword(contrasena: password), "identificacionDelantera": self.ConvertImageToBase64String(img: foto1), "identificacionTrasera": self.ConvertImageToBase64String(img:
    foto2),"tipoIdentificacion": tipoIdentificacion])
            
            self.showActivityIndicatory(uiView: self.view)
            
            agregarUsuario(usuario: usuario, completions: { success, message  in
                print("message completions -> \(message)")
                DispatchQueue.main.async {
                
                self.hideActivityIndicator(uiView: self.view)
                if success {
                   
                    self.accion(title: "Usuario", message: message , actionButton: {
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let control = story.instantiateViewController(identifier: "inicio") as! ViewController
                        self.present(control,animated: true, completion: nil)
                        //unwind segue
                                    })
                } else {
                    
                    self.basica(title: "Atención", message: message)
                    
                }
            }
        })
    }
}
    
    /**{ respuesta, message  in
                print("message completions -> \(message)")
                if respuesta {
                    DispatchQueue.main.async {
                    self.alertaBasicaAction(title: "Usuario", message: "Usted ha sido registrado EXITOSAMENTE", actionButton: {
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let control = story.instantiateViewController(identifier: "inicio") as! ViewController
                    self.present(control,animated: true, completion: nil)
                    })
                }
                } else {
                        self.alertaBasicaAction(title: "Atenciòn", message: "Hubo un error en su Registro", actionButton: {
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let control = story.instantiateViewController(identifier: "inicio") as! ViewController
                        self.present(control,animated: true, completion: nil)

                        })
       }
    }*/
    
    func validar(nombre:String, apellido_pat: String, apellido_mat: String, correo: String, contraseña: String, imagen: UIImage) -> Bool {
        
        
        guard let nombre = self.txtNombre.text, nombre != "" else {
            self.basica(title: "Atención", message: "Complete el campo nombre")
           return false }

        guard nombre.count >= 3,nombre.validarTexto else {
        self.basica(title: "Atención", message: "No es un nombre valido")
            return false }

        guard let apaterno = self.txtApellidoPaterno.text, apaterno != "" else {
            self.basica(title: "Atención", message: "Complete el campo apellido paterno")
            return false }
        guard apaterno.validarTexto else {
            self.basica(title: "Atención", message: "El apellido paterno no es valido")
                      return false }
        
        guard let amaterno = self.txtApellidoMaterno.text else { return false }
        
        guard let correo = self.txtCorreo.text, correo != "" else {
            self.basica(title: "Atención", message: "Complete el campo correo")
            return false }

        guard correo.validarEmail else {
            self.basica(title: "Atención", message: "El correo es invalido")
            return false }
        
        
        guard let contraseña = self.txtContraseña.text, contraseña != "" else {
             self.basica(title: "Atención", message: "Complete el campo contraseña")
             return false }
        
        guard contraseña.count >= 8 else {
        self.basica(title: "Atención", message: "son 8 caracteres minimos en la contraseña")
        return false }
        
        guard let confirmar = self.txtConfirmar.text, confirmar != "",confirmar.count >= 8,confirmar == contraseña else {
            self.basica(title: "Atención", message: "Las contraseñas no coinciden")
             return false }
        
       //guard let imagen = self.imgFoto1.image, imagen != image else {return false}
        //self.basica(title: "Atencion", message: "debe elegir alguna imagen")
        
            return true
        }
    
    
    
    func resize(_ image: UIImage) -> UIImage{

        let ancho:CGFloat = image.size.width
        let altura:CGFloat = image.size.height
        let imgrelacion:CGFloat = ancho/altura
        let maximoancho:CGFloat = 640 // coonstante para asegurarnos del tamaño
        let nuevoalto:CGFloat = maximoancho/imgrelacion
        let constantedecompresion:CGFloat = 0.5 //
        //en el 4to cuadrante 0 y 0
        let rect: CGRect = CGRect(x: 0, y: 0, width: maximoancho, height: nuevoalto)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData: Data = img.jpegData(compressionQuality: constantedecompresion)!
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData)!
    }

    
  func cifrarPassword(contrasena:String) -> String{
       let password = Data(contrasena.utf8)
        let has = SHA256.hash(data: password)
       return has.compactMap { String(format: "%02x", $0) }.joined()
   }    
    func ConvertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }
   }
