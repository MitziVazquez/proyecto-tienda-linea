//
//  API.swift
//  ComprasLinea
//
//  Created by MAC on 12/21/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

enum Path: String {
    case agregarUsuario = "/registrodeusuario"
    case agregarCliente = "/registrarCliente"
    case login = "/iniciarSesionCliente"
    case listaProductos = "/verProducto"
    case domicilio = "/registrarDomicilio"
}

//let base:String = "http://10.95.71.22:8070/api/bdm/tiendaEnLinea"
//let base:String = "http://10.95.71.22:8080/tiendaenlinea"
//let base: String = "http://10.95.71.57:8080/servicios/api/bdm/tiendapp"

let base: String = "http://192.168.15.57:8070/api/bdm/tiendaEnLinea"

let config = URLSessionConfiguration.default
let session = URLSession(configuration: config)
weak var alerta: ViewController?

func agregarUsuario(usuario: Usuario, completions: @escaping (Bool,String) -> ()){
    guard let url = URL(string: "\(base)\(Path.agregarCliente.rawValue)") else {return}
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    
    let parameters:[String:String] = ["nombre": usuario.nombre, "apellidoPaterno": usuario.apellido_pat, "apellidoMaterno": usuario.apellido_mat, "correo": usuario.correo, "password": usuario.contraseña,"identificacionDelantera": usuario.foto1, "identificacionTrasera": usuario.foto2, "tipoIdentificacion": usuario.validarIdentificacion]
    
    guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
    request.httpBody = httpBody

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        
        guard let data = data else {
            completions (false, "No conexion con el servidor")
            print ("Error en los datos")
            return }
        
        guard error == nil else {
            completions(false, "Error en el servidor")
            return
        }
        
        if let datos = String(data: data, encoding: .utf8) {
            print(datos)
            print(response)
        }
        
        guard let json = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? [String : Any] else {
            print("No es un tipo json")
            return
        }
        print(json)
        
        guard let codigo = json["codigoOperacion"] as? String, let mensaje = json["msj"] as? String else {
                print("********* problemas para sacar el codigo")
                return}
        
        print("*********\(codigo)")
        
        if codigo == "1" {
            
            completions(true, mensaje)
            print("Exito")

        }
        if codigo == "0" {
           completions (false, mensaje)
            print("Error")
    }
        
        print("json -> \(json)")
    }

    task.resume()
}

func iniciarSesion(ingresar: Ingresar, completions: @escaping (Bool,String) -> ()){
    
    guard let url = URL(string: "\(base)\(Path.login.rawValue)") else {return}
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    
    let parameters:[String:String] = ["correo": ingresar.nombreUsuario, "password": ingresar.contrasena]
    guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                completions (false, "Tenemos un ERROR en el sevidor")
                print ("Error en los datos")
                return }
            
            guard error == nil else {
               completions (false, "Error en el servidor")
                print("Error nil")
                return
            }
            
            if let datos = String(data: data, encoding: .utf8) {
                print(datos)
                print(response)
            }
        
            guard let json = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? [String : Any] else {
                print("No es un tipo json")
                return
            }
            print(json)
            
            guard let codigo = json["codigoOperacion"] as? String, let mensaje = json["msj"]  as? String else {return}
            
            if codigo == "1" {
                DispatchQueue.main.async {
                completions(true, mensaje)
                print("Exito")
            }
        }

            if codigo == "0" {
               DispatchQueue.main.async {
               completions (false, mensaje)
                print("Error")
            }
        }
            
            print("json -> \(json)")
        }

        task.resume()
    }

func Producto(completions: @escaping ([Product]) -> ()) {
    guard let url = URL(string: "\(base)\(Path.listaProductos.rawValue)") else {return}
    var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    let parameters:[String:String] = ["peticionProductos":"1"]
   guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
    request.httpBody = httpBody

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        
        guard let data = data else {
            print ("Error en los datos")
            return }
        
        guard error == nil else {
            print("Error nil")
            return
        }
        
        if let datos = String(data: data, encoding: .utf8) {
            print(response)
        }
        guard let json = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] else {
            print("No es un tipo json")
            return
        }
        
        
        var list = [Product]()
        for producto in (json["listaProductos"] as? [Dictionary<String, Any>])! {
            list.append(Product(dic: producto))
                   
        }
            
        completions(list)
                    
  }

   task.resume()
               
}
func agregarDomicilio(domicilio: Domicilio, completions: @escaping (Bool,String) -> ()){
    guard let url = URL(string: "\(base)\(Path.domicilio.rawValue)") else {return}
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    
    let parameters:[String:String] = ["calle": domicilio.calle,"numeroInterior": domicilio.numInterior,"numeroExterior": domicilio.numExterior,"codigoPostal": domicilio.codigoPostal,"municipio": domicilio.municipio,"colonia": domicilio.colonia,"ciudad": domicilio.ciudad]
    
    guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
    request.httpBody = httpBody

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        
        guard let data = data else {
            completions (false, "No conexion con el servidor")
            print ("Error en los datos")
            return }
        
        guard error == nil else {
            completions(false, "Error en el servidor")
            return
        }
        
        if let datos = String(data: data, encoding: .utf8) {
            print(datos)
            print(response)
        }
        
        guard let json = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? [String : Any] else {
            print("No es un tipo json")
            return
        }
        print(json)
        
        guard let codigo = json["codigoOperacion"] as? String, let mensaje = json["mensaje"] as? String else {
                print("********* problemas para sacar el codigo")
                return}
        
        print("*********\(codigo)")
        
        if codigo == "1" {
            
            completions(true, mensaje)
            print("Exito")

        }
        if codigo == "0" {
           completions (false, mensaje)
            print("Error")
    }
        
        print("json -> \(json)")
    }

    task.resume()
}
