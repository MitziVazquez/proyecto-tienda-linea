//
//  Caracteres.swift
//  ComprasLinea
//
//  Created by MAC on 12/24/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

extension String {
    var validarEmail: Bool {
        let emailRegex = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailTest = NSPredicate (format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: self)
}
    var validarTexto: Bool {
            let textRegex = "(\\S[A-ZÑÁÉÍÓÚÜ\\sa-zñáéíóúü\\s]{3,50})"
            let textTest = NSPredicate(format: "SELF MATCHES %@", textRegex)
            return textTest.evaluate(with: self)
        }
    var validarNumero: Bool {
        let numeroRegex = "([0-9\\s]*)"
        let numeroTest = NSPredicate(format: "SELF MATCHES %@", numeroRegex)
        return numeroTest.evaluate(with: self)
    }
}

