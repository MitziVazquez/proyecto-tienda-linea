//
//  Alertas.swift
//  ComprasLinea
//
//  Created by MAC on 12/22/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

var container: UIView = UIView()
var loadingView: UIView = UIView()
var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

extension UIViewController {
    
    func basica (title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func accion (title:String, message:String, actionButton: @escaping () -> ()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            DispatchQueue.main.async {
                actionButton()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showActivityIndicatory(uiView: UIView) {
           container.frame = uiView.frame
           container.center = uiView.center
        container.backgroundColor = UIColor.lightGray

           loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
           loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.lightGray
           loadingView.clipsToBounds = true
           loadingView.layer.cornerRadius = 10
           
           activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
           activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
           activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2,
                                   y: loadingView.frame.size.height / 2);
           
           loadingView.addSubview(activityIndicator)
           container.addSubview(loadingView)
           uiView.addSubview(container)
           activityIndicator.startAnimating()
       }
       
       // Oculta el indicador de actividades
       func hideActivityIndicator(uiView: UIView) {
           activityIndicator.stopAnimating()
           container.removeFromSuperview()
       }
}
     

