//
//  Modelo.swift
//  ComprasLinea
//
//  Created by MAC on 12/21/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

struct Usuario: Decodable {
    
    var nombre: String
    var apellido_pat: String
    var apellido_mat: String
    var correo: String
    var contraseña: String
    var validarIdentificacion: String
    var foto1: String
    var foto2: String
    
 
  init(dic: [String: Any]) {

    self.nombre = dic["nombre"] as? String ?? ""
    self.apellido_pat = dic["apellidoPaterno"] as? String ?? ""
    self.apellido_mat = dic["apellidoMaterno"] as? String ?? ""
    self.correo = dic["correo"] as? String ?? ""
    self.contraseña = dic["password"] as? String ?? ""
    self.validarIdentificacion = dic["tipoIdentificacion"] as? String ?? ""
    self.foto1 = dic["identificacionDelantera"] as? String ?? ""
    self.foto2 = dic["identificacionTrasera"] as? String ?? ""
   
    }
}
