//
//  ModeloLogin.swift
//  ComprasLinea
//
//  Created by MAC on 12/24/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

struct Ingresar {
    
    var nombreUsuario: String
    var contrasena: String
    
  init(dic: [String: Any]) {
    
    self.nombreUsuario = dic["correo"] as? String ?? ""
    self.contrasena = dic["password"] as? String ?? ""
   
    }
}
