//
//  ModeloDomicilio.swift
//  ComprasLinea
//
//  Created by MAC on 1/3/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

struct Domicilio: Decodable {
   
    var calle: String
    var numExterior: String
    var numInterior: String
    var codigoPostal: String
    var municipio: String
    var colonia: String
    var ciudad: String
    
 init(dic: [String: Any]) {

    self.calle = dic["calle"] as? String ?? ""
    self.numExterior = dic["numeroExterior"] as? String ?? ""
    self.numInterior = dic["numeroInterior"] as?  String ?? ""
    self.codigoPostal = dic["codigoPostal"] as? String ?? ""
    self.municipio = dic["municipio"] as? String ?? ""
    self.colonia = dic["colonia"] as? String ?? ""
    self.ciudad = dic["ciudad"] as? String ?? ""
    }
    
}
