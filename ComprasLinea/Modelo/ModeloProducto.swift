//
//  ModeloProducto.swift
//  ComprasLinea
//
//  Created by MAC on 12/29/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

struct Product {
   

    var tipoDeporte: Int
    var nombreProducto: String
    var img: String
    var descripcion: String
    var precio: Float
    var descuento: Float
    var stock: Int

  
    init(dic: [String: Any]) {
        self.tipoDeporte = dic["tipoDeporte"] as? Int ?? 0
        self.nombreProducto = dic["nombreProducto"] as? String ?? ""
        self.img = dic["img"] as? String ?? ""
        self.descripcion = dic["descripcion"] as? String ?? ""
        self.precio = dic["precio"] as? Float ?? 0.0
        self.descuento = dic["descuento"] as? Float ?? 0.0
        self.stock = dic["stock"] as? Int ?? 0
        
  }
}
